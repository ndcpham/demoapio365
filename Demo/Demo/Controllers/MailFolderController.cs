﻿using Demo.Helpers;
using Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Demo.Controllers
{
    
    public class MailFolderController : BaseController
    {
        [Authorize]
        // GET: MailFolder
        public async Task<ActionResult> Index()
        {
            var mailFolder = await GraphHelper.GetMailFoldersAsync();
           
            return View(mailFolder);
        }
        public async Task<ActionResult> List_Messege()
        {
            var list_mess = await GraphHelper.GetMessage();
            return View(list_mess);
        }

        public ActionResult Create_Message()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> CreateMessage(Messeges ms)
        {
            await GraphHelper.Create_Messege(ms);
            return  RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<ActionResult> Send_Message(string id)
        {
             GraphHelper.Send_Message(id);
            return RedirectToAction("List_Messege");
        }
        public ActionResult Send_Mail()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> SendMail(Messeges ms)
        {
            GraphHelper.Send_Mail(ms);
            return RedirectToAction("Index");
        }
    }
}