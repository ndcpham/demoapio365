﻿using Demo.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Demo.Controllers
{
    public class UsersController : BaseController
    {

        // GET: Users
        [Authorize]
        public async Task<ActionResult> Index()
        {
            var list_User = await GraphHelper.List_User();
            return View(list_User);
        }
    }
}