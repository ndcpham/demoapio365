﻿using Demo.Helpers;
using Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Demo.Controllers
{
    public class CalendarController : BaseController
    {
        // GET: Calendar
        [Authorize]
        public async Task<ActionResult> Index()
        {
            var events = await GraphHelper.GetEventsAsync();

            // Change start and end dates from UTC to local time
            foreach (var ev in events)
            {
                ev.Start.DateTime = DateTime.Parse(ev.Start.DateTime).ToLocalTime().ToString();
                ev.Start.TimeZone = TimeZoneInfo.Local.Id;
                ev.End.DateTime = DateTime.Parse(ev.End.DateTime).ToLocalTime().ToString();
                ev.End.TimeZone = TimeZoneInfo.Local.Id;
            }

            return View(events);
        }
        
        public ActionResult Create_calendar()
        {          
            return View();
        }  
        [HttpPost]
        public async Task<ActionResult> Createcalendar(CalendarEvent events)
        {
            await GraphHelper.CreateEvent(events);
            return RedirectToAction("Index");
       
        }
        [HttpPost,ActionName("Delete")]
        public async Task<ActionResult> Delete_event(string id)
        {
            GraphHelper.Del_event(id);
            return RedirectToAction("Index");
        }
    }
}