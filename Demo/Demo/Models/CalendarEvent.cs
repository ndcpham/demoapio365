﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Demo.Models
{
    public class CalendarEvent
    {
        public class Body
        {
            public string ContentType { get; set; }
            public string Content { get; set; }

        }
        public class EventDateTime
        {
            public string DateTime { get; set; }
            public string TimeZone { get; set; }
        }

        public string Subject { get; set; }
        public Body body { get; set; }
        public EventDateTime Start { get; set; }
        public EventDateTime End { get; set; }

        public CalendarEvent()
        {
            this.body = new Body()
            {
                ContentType = "html"
            };
            this.Start = new EventDateTime()
            {
                TimeZone = "Pacific Standard Time"
            };
            this.End = new EventDateTime()
            {
                TimeZone = "Pacific Standard Time"
            };
        }
    }
}