﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Models
{
    public class Messeges
    {
        public string Subject { get; set; }
        public Body body { get; set; }
        public EmailAddress emailAddress { get; set; }

        public class Body
        {
            public string ContentType { get; set; }
            public string Content { get; set; }
        }

        public class EmailAddress
        {
            public string Addressto { get; set; }
            public string Addresscc { get; set; }
            public string name { get; set; }
            
        }
        public Messeges()
        {
            this.body = new Body
            {
                ContentType = "html"
            };
        }
    }
}