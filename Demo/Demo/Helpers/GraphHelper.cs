﻿using Demo.Models;
using Demo.TokenStorage;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Demo.Helpers
{
    public class GraphHelper
    {
        public static async Task<CachedUser> GetUserDetailsAsync(string accessToken)
        {
            var graphClient = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    async (requestMessage) =>
                    {
                        requestMessage.Headers.Authorization =
                            new AuthenticationHeaderValue("Bearer", accessToken);
                    }));

            var user = await graphClient.Me.Request()
                .Select(u => new
                {
                    u.DisplayName,
                    u.Mail,
                    u.UserPrincipalName
                })
                .GetAsync();

            return new CachedUser
            {
                Avatar = string.Empty,
                DisplayName = user.DisplayName,
                Email = string.IsNullOrEmpty(user.Mail) ?
                    user.UserPrincipalName : user.Mail
            };
        }
        // Load configuration settings from PrivateSettings.config
        private static string appId = ConfigurationManager.AppSettings["ida:AppId"];
        private static string appSecret = ConfigurationManager.AppSettings["ida:AppSecret"];
        private static string redirectUri = ConfigurationManager.AppSettings["ida:RedirectUri"];
        private static List<string> graphScopes =  new List<string>(ConfigurationManager.AppSettings["ida:AppScopes"].Split(' '));

        public static async Task<IEnumerable<User>> List_User()
        {
            var graphClient = GetAuthenticatedClient();
            var users = await graphClient.Users
                        .Request()
                        .GetAsync();
            return users.CurrentPage;
        }
        public static async Task<IEnumerable<Event>> GetEventsAsync()
        {
            var graphClient = GetAuthenticatedClient();

            var events = await graphClient.Me.Events.Request()
                .Select("subject,organizer,start,end")
                .OrderBy("createdDateTime DESC")
                .GetAsync();

            return events.CurrentPage;
        }

        public static async Task<Event> CreateEvent(CalendarEvent ev)
        {
            var graphClient = GetAuthenticatedClient();

            var @event = new Event
            {

                Subject = ev.Subject,
                Body = new ItemBody
                {
                    ContentType = BodyType.Html,
                    Content = ev.body.Content
                },
                Start = new DateTimeTimeZone
                {
                    DateTime = ev.Start.DateTime,
                    TimeZone = ev.Start.TimeZone
                },
                End = new DateTimeTimeZone
                {
                    DateTime = ev.End.DateTime,
                    TimeZone = ev.End.TimeZone
                }
            };
            return await graphClient.Me.Events
                   .Request()
                   .AddAsync(@event);
        }

        public static async void Del_event(string id)
        {
            var graphClient = GetAuthenticatedClient();
             await graphClient.Me.Events[id]
                        .Request()
                        .DeleteAsync();
        }
        public static async Task<IEnumerable<MailFolder>> GetMailFoldersAsync()
        {
            var graphClient = GetAuthenticatedClient();

            var mailFolders = await graphClient.Me.MailFolders
                             .Request().Select("displayName,childFolderCount,unreadItemCount,totalItemCount")
                             .GetAsync();
            return mailFolders.CurrentPage;
        }
        public static async Task<IEnumerable<Message>> GetMessage()
        {
            var graphClient = GetAuthenticatedClient();

            var messages = await graphClient.Me.MailFolders["AAMkAGNlNGI3YTY1LTc1NmItNDYwMC05NWJiLWMyNzk5NTcyMTQwMQAuAAAAAAD4FDHI7NT-S5zDUKzJ3KqoAQCQ9tvxU9oURanJ89mIIUvOAAAAAAEPAAA="].Messages
                             .Request()
                             .Select("Subject,Sender")
                             .GetAsync();
            return messages.CurrentPage;
        }
        public static async Task<Message> Create_Messege(Messeges ms)
        {
            var graphClient = GetAuthenticatedClient();
            var message = new Message
            {
                Subject = ms.Subject,
                Importance = Importance.Low,
                Body = new ItemBody
                {
                    ContentType = BodyType.Html,
                    Content = ms.body.Content
                },
                ToRecipients = new List<Recipient>()
                {
                    new Recipient
                    {
                        EmailAddress = new EmailAddress
                        {
                            Address = ms.emailAddress.Addressto,                
                        }
                    }
                },            
            };
            return await graphClient.Me.MailFolders["AAMkAGNlNGI3YTY1LTc1NmItNDYwMC05NWJiLWMyNzk5NTcyMTQwMQAuAAAAAAD4FDHI7NT-S5zDUKzJ3KqoAQCQ9tvxU9oURanJ89mIIUvOAAAAAAEPAAA="].Messages
                 .Request()
                 .AddAsync(message);
        }
        public static async void Send_Message(string id)
        {
            var graphClient =  GetAuthenticatedClient();

             await graphClient.Me.Messages[id]
                .Send()
                .Request()
                .PostAsync();
        }

        public static async void Send_Mail(Messeges ms)
        {
            var graphClient =  GetAuthenticatedClient();

            var message = new Message
            {
                Subject = ms.Subject,
                Body = new ItemBody
                {
                    ContentType = BodyType.Text,
                    Content = ms.body.Content
                },
                ToRecipients = new List<Recipient>()
                {
                    new Recipient
                    {
                        EmailAddress = new EmailAddress
                        {
                            Address = ms.emailAddress.Addressto
                        }
                    }
                }
            };
            var saveToSentItems = false;

         await graphClient.Me
                .SendMail(message, saveToSentItems)
                .Request()
                .PostAsync();
        }
        private static GraphServiceClient GetAuthenticatedClient()
        {
            return new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    async (requestMessage) =>
                    {
                        var idClient = ConfidentialClientApplicationBuilder.Create(appId)
                            .WithRedirectUri(redirectUri)
                            .WithClientSecret(appSecret)
                            .Build();

                        var tokenStore = new SessionTokenStore(idClient.UserTokenCache,
                                HttpContext.Current, ClaimsPrincipal.Current);

                        var userUniqueId = tokenStore.GetUsersUniqueId(ClaimsPrincipal.Current);
                        var account = await idClient.GetAccountAsync(userUniqueId);

                        // By calling this here, the token can be refreshed
                        // if it's expired right before the Graph call is made
                        var result = await idClient.AcquireTokenSilent(graphScopes, account)
                                    .ExecuteAsync();

                        requestMessage.Headers.Authorization =
                            new AuthenticationHeaderValue("Bearer", result.AccessToken);
                    }));
        }
    }
}
